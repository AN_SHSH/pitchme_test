import asyncpg


class DatabaseConnection(object):
    def __init__(self, dns: dict):
        self.connection = None
        self.dns = dns

    async def __aenter__(self) -> object:
        self.connection = await asyncpg.connect(**self.dns)
        return self.connection

    async def __aexit__(self, exc_type: str, exc_val: str, exc_tb: str) -> None:
        await self.connection.close()  # type: ignore
