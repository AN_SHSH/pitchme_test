__version__ = 1.00

from app.web_server import ServerCityEvents
import os


init_data: dict = {
    "server_port": os.environ.get("server_port"),
    "postgres": {
        "user": os.environ.get("postgres_login"),
        "password": os.environ.get("postgres_pass"),
        "host": os.environ.get("postgres_host"),
        "database": os.environ.get("postgres_db"),
        "port": os.environ.get("postgres_port"),
    },
}

server: object = ServerCityEvents(**init_data)
