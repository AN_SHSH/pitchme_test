from aiohttp import web
import jinja2
import aiohttp_jinja2

from datetime import datetime

from app.apg import DatabaseConnection


class ServerCityEvents:
    def __init__(self, **env: dict):
        self.env = env

        self.app = web.Application()

        self.app["static_root_url"] = "/static"

        aiohttp_jinja2.setup(
            self.app, loader=jinja2.FileSystemLoader("./app/templates/")
        )

        self.app.router.add_static("/static", path="./app/static/", name="static")

        self.app.add_routes(
            [
                web.get("/", self.first_page),
                web.get("/search", self.search),
                web.post("/add/filter_user", self.add_filter_user),
            ]
        )

        web.run_app(self.app, port=9090)

    @aiohttp_jinja2.template("first_page.jinja2")
    async def first_page(self, request: object) -> dict:

        async with DatabaseConnection(self.env["postgres"]) as conn:
            events: list = await conn.fetch("SELECT * FROM sity_events.events")
            theme_events: list = await conn.fetch(
                "SELECT * FROM sity_events.theme_events"
            )

        page = {
            "page": {"title": "События", "events": events, "theme_events": theme_events}
        }

        return page

    async def add_filter_user(self, request: object) -> object:
        get_request = await request.json()

        async with DatabaseConnection(self.env["postgres"]) as conn:

            prepare_filters_user = await conn.prepare(
                "INSERT INTO sity_events.users (fingerprint, city, start_time, title) VALUES ($1, $2, $3, $4)"
            )
            await prepare_filters_user.fetch(
                get_request["id"],
                get_request["filter"]["city"],
                datetime.strptime(get_request["filter"]["time"], "%H:%M:%S"),
                get_request["filter"]["title"],
            )
        return web.Response(status=200)

    async def search(self, request: object) -> object:
        get_request = request.rel_url.query

        query_filter = True
        for i in get_request.values():
            if not i:
                query_filter = False
            else:
                query_filter = True
                break

        async with DatabaseConnection(self.env["postgres"]) as conn:

            if query_filter:
                if get_request["time"]:
                    events_prepare: object = await conn.prepare(
                        """
                    SELECT
                        *, to_char(start_time, 'HH24:MI:SS') AS start_time, to_char(end_time, 'HH:MM:SS') AS end_time,
                            to_char(start_date, 'yyyy-mm-dd') AS start_date, to_char(end_date, 'yyyy-mm-dd') AS end_date
                    from sity_events.events WHERE (city = $1 OR title = $2 OR start_time = $3)"""
                    )
                    events_result: list = await events_prepare.fetch(
                        get_request["city"],
                        get_request["title"],
                        datetime.strptime(get_request["time"], "%H:%M:%S"),
                    )
                else:
                    events_prepare: object = await conn.prepare(
                        """
                    SELECT
                        *, to_char(start_time, 'HH24:MI:SS') AS start_time, to_char(end_time, 'HH:MM:SS') AS end_time,
                        to_char(start_date, 'yyyy-mm-dd') AS start_date, to_char(end_date, 'yyyy-mm-dd') AS end_date
                    FROM sity_events.events WHERE (city = $1 OR title = $2)"""
                    )

                    events_result: list = await events_prepare.fetch(
                        get_request["city"], get_request["title"]
                    )
            else:
                events_result: object = await conn.fetch(
                    """
                    SELECT
                        *, to_char(start_time, 'HH24:MI:SS') AS start_time, to_char(end_time, 'HH:MM:SS') AS end_time,
                        to_char(start_date, 'yyyy-mm-dd') AS start_date, to_char(end_date, 'yyyy-mm-dd') AS end_date
                    FROM sity_events.events """
                )

            theme_events: list = await conn.fetch(
                "SELECT * FROM sity_events.theme_events"
            )

            def events_result_serializable(data):
                result = []

                for i in data:
                    result.append(dict(i))

                return result

        page = {
            "page": {
                "events": events_result_serializable(events_result),
                "theme_events": events_result_serializable(theme_events),
            }
        }

        return web.json_response(page)
