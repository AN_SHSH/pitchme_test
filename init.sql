create schema if not exists sity_events;

create table if not exists sity_events.events
(
    eid SERIAL PRIMARY KEY,
    title text NOT NULL,
    description text NOT NULL,
    city text NOT NULL,
    start_time time NOT NULL,
    end_time time NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL
);

create table if not exists sity_events.users
(
    fingerprint text NOT NULL,
    email text NULL,
    city text NULL,
    start_time time NULL,
    title text NULL,
    UNIQUE (fingerprint)
);

create table if not exists sity_events.theme_events
(
    eid integer NOT NULL,
    cid integer NOT NULL,
    title_theme text NOT NULL
);

create table if not exists sity_events.chat_events
(
    eid integer NOT NULL,
    cid integer NOT NULL,
    uid integer NOT NULL,
    message text NOT NULL
);

INSERT INTO sity_events.events
    (title, description, city, start_time, end_time, start_date, end_date)
VALUES
    ('День Города', 'Москва отмечает свой 873-й день рождения. Празднование Дня города пройдет традиционно в первые выходные осени, 5 и 6 сентября.', 'Москва', '14:00', '20:00', '2020.09.05', '2020.09.06 ');

INSERT INTO sity_events.events
    (title, description, city, start_time, end_time, start_date, end_date)
VALUES
    ('Праздник Алые Паруса в Санкт-Петербурге', 'В 2020 году в мае традиционно все российские выпускники с нетерпением ждут важного события в жизни - выпускного. Позади школьная скамья, экзамены. Впереди взрослая и самостоятельная жизнь, полная надежд и чаяний. Именно поэтому этот праздник для многих выпускников становится отправной точкой во взрослую жизнь. И хочется, чтобы этот день запомнился надолго. В 2020 году праздник запланирован в ночь с 20 на 21 июня.', 'Санкт-Петербург', '22:30', '01:00', '2020.06.20', '2020.06.21');


INSERT INTO sity_events.theme_events
    (eid, cid, title_theme)
VALUES
    (1, 1, 'Каких исполнителей Вы бы хотели видеть на концерте?');

INSERT INTO sity_events.theme_events
    (eid, cid, title_theme)
VALUES
    (1, 2, 'Откуда открывается лучший вид на салют?');


INSERT INTO sity_events.theme_events
    (eid, cid, title_theme)
VALUES
    (2, 1, 'Стоит ли идти на Алые Паруса?');

INSERT INTO sity_events.theme_events
    (eid, cid, title_theme)
VALUES
    (2, 2, 'Из какого города Вы приедите на Алые Паруса?');