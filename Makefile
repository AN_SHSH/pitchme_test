NAME=app

init:
	pip install pipenv --upgrade
	pipenv install --dev

flake8: init
	pipenv run flake8 ./$(NAME)

bandit: init
	pipenv run bandit -r ./$(NAME)

format: init
	pipenv run black $(NAME) -S -l 79

lint: flake8 bandit


clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
